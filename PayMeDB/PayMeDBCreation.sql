﻿use master
go


create database PayMeDB

go
use  PayMeDB
go

create schema Info

go


create table Info.[Users](

	[UserID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	FirstName nvarchar(30) not null,
	LastName nvarchar(30) not null,
	SSN int not null unique,
	[Password] nvarchar(20) NOT NULL,
	[Email] nvarchar(50) NOT NULL unique,
	[Address] nvarchar(100) null,
	[Country] nvarchar(25) not null,
	[ActivationCode] [int] NOT NULL,
	[isActive] [bit] NOT NULL default 1,--user is active or not,
	[BirthDate] [datetime]  NULL,
	[PicturePath] nvarchar(150) NULL,--Profil picture url
	[RegisterDate] [datetime] NULL,
	PhoneNumber nvarchar(20)  null,
	AccountLimit nvarchar(20)  not null default 'PERSONAL'
	
	)

	Create table [Info].[TimeZones](
	UserID	int not null foreign key references Info.Users(UserID),
	[TimeZoneID] int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[TimeZoneValue] nvarchar(100) Not Null unique
)

create table Info.InformationIs(
	
	UserID int not null foreign key references Info.Users(UserID),
	IsMail bit not null default 0,
	IsPhone bit not null default 0,
	InfoContext nvarchar(100) not null /*mail and phone context for security*/
)

create table Info.TransactionType(
	
	TransactionTypeID int not null primary key identity(1,1),
	TransactionType nvarchar(30) not null
)

create table Info.Currency(

	CurrencyID int not null primary key identity(1,1),
	CurrencyType char(5) not null,	
	ForeignCurrencyAmount decimal(12,6) not null
)

create table Info.[Transaction](

	TransactionID int not null primary key IDENTITY(1,1),
	Balance decimal(12,6) not null,
	TransactionTypeID int foreign key references Info.TransactionType(TransactionTypeID),
	ReceiverID int NOT NULL foreign key references [Info].[Users]([UserID]),
	SenderID int NOT NULL foreign key references [Info].[Users]([UserID]),
	CurrencyID int foreign key references [Info].[Currency](CurrencyID),
	TransactionTime datetime not null

)





create table Info.DoneTransaction(
	DoneTransactionID int primary key identity(1,1),
	TransactionID int foreign key references Info.[Transaction](TransactionID),
	TransactionTime datetime,
	TransactionError nvarchar(100) null,
	TransactionAmount decimal(12,6) not null
)




create table Info.CardInfo(
CardInfoID int not null primary key identity(1,1),
UserID int not null foreign key references Info.Users(UserID),
cardNumber nvarchar(16) null,
[expireDate] nvarchar(8) null,
cvv int null

)


create table Info.BankAccountInfo(
BankAccountInfoID int not null primary key identity(1,1),
UserID int not null foreign key references Info.Users(UserID),
BankName nvarchar(50) null,
AccountNumber nvarchar(30)

)


create table Info.Receipt(
	receiptID int not null identity(1,1) primary key,
	receiverID int not null foreign key references Info.Users(UserID),
	senderID int not null foreign key references Info.Users(UserID),
	receiptNumber nvarchar(12) not null ,
	receiptDate datetime not null,
	receiptDescription nvarchar(100) not null,
	SSN int references Info.Users(SSN),
	[TransactionID]	int not null foreign key references [Info].[Transaction]([TransactionID])
)

GO

create table Info.SecurityQuest(
	SecurityQuest int not null primary key identity(1,1),
	UserID int not null foreign key references Info.Users(UserID),
	SecurityQuestion nvarchar(100) not null,
	SecurityAnswer nvarchar(30) not null
)
go



create table Info.ContactUs(
ContactUs int not null primary key identity(1,1),
mailtrapHostMail nvarchar(50) null,
[Name] nvarchar(50) null,
[Email] nvarchar(50) null,
PhoneNumber nvarchar(30)  null,
[Message] nvarchar(500) null
)
go


create table Info.DoneTransactionType (
DoneTransactionTypeID int not null primary key identity(1,1),
TransactionID int foreign key references Info.[Transaction](TransactionID),
UserID int not null foreign key references Info.Users(UserID),
DoneTransactionType nvarchar(30)  not null
)


select [mailtrapHostMail] from [Info].[ContactUs]
go

INSERT INTO [Info].[ContactUs] ([mailtrapHostMail])VALUES('onursanli05@gmail.com')
INSERT INTO [Info].[ContactUs] ([mailtrapHostMail])VALUES('masumcelilolgun@gmail.com')
go


create procedure Info.SaveContactUs(
@Name nvarchar(50) ,
@Email nvarchar(50) ,
@PhoneNumber nvarchar(30)  ,
@Message nvarchar(500)
)
as INSERT INTO [Info].[ContactUs]([Name],[Email],[PhoneNumber],[Message])
VALUES(@Name,@Email,@PhoneNumber,@Message)
GO

create table Info.Invoice(
[InvoiceID] int IDENTITY(1,1) NOT NULL PRIMARY KEY,
DoneTransactionTypeID int not null,
UserID int not null foreign key references Info.Users(UserID),
SenderFirstName nvarchar(30) not null,
SenderLastName nvarchar(30) not null,
SenderSSN int not null,
ReceiverFirstName nvarchar(30) not null,
ReceiverLastName nvarchar(30) not null,
ReceiverSSN int not null,
DoneTransactionType nvarchar(30)  not null,
TransactionAmount decimal(12,6) not null,
TransactionTime datetime not null,
InvoiceControl nvarchar(15) not null,
CurrencyType char(5) not null

)
go


--register user
create procedure Info.RegisterUser(
@FirstName nvarchar(30),
@LastName nvarchar(30),
@SSN int,
@Password [nvarchar](20),
@Email [nvarchar](50),
@Address nvarchar(100),
@Country nvarchar(25),
@BirthDate [datetime],
@TimeZone nvarchar(100),
@SecurityQ nvarchar(100),
@SecurityAns nvarchar(30)

)
 as
INSERT INTO Info.[Users]
 (FirstName,
LastName,
SSN,
[Password],
Email,
[Address],
[Country],
ActivationCode,
isActive,
BirthDate,
PicturePath,
RegisterDate,
PhoneNumber
)
     VALUES
(@FirstName,
@LastName,
@SSN,
@Password,
@Email,
@Address,
@Country,
0,
1,
@BirthDate,
null,
GETDATE(),
null
)
declare @userId int
select @userId=SCOPE_IDENTITY();
INSERT INTO Info.TimeZones (UserID,TimeZoneValue) values(@userId,@TimeZone)
INSERT INTO  [Info].[SecurityQuest](UserID,[SecurityQuestion],SecurityAnswer) values(@userId,@SecurityQ,@SecurityAns)

GO







go
 --register users
 exec Info.RegisterUser 'MASUM','OLGUN',123,'PWD','ema1@gmail.com','ADRS1','country1',0,1111,1,'s'

 update [Info].[Users] set [AccountLimit]='BUSINESS' WHERE [UserID]=1
 GO
 exec Info.RegisterUser 'ONUR','SANLI',124,'PWD2','ema2@gmail.com','ADRS2','country2',0,2222,1,'s'
 go
  exec Info.RegisterUser 'ONUR1','SANLI1',1241,'PWD','ema3@gmail.com','ADRS32','country2',0,22222,1,'s'
 go
  update [Info].[Users] set [AccountLimit]='BUSINESS' WHERE [UserID]=3
 GO
 --insert to currency
 INSERT INTO  [Info].Currency([CurrencyType],ForeignCurrencyAmount)VALUES('TR',1)
 INSERT INTO  [Info].Currency([CurrencyType],ForeignCurrencyAmount)VALUES('USD',2)
 INSERT INTO  [Info].Currency([CurrencyType],ForeignCurrencyAmount)VALUES('EU',3)
 
 GO

 --17.12.2016
create procedure Info.SaveBankInformation(
@UserID int,@BankName nvarchar(50),@AccountNumber nvarchar(30))	

as
INSERT INTO [Info].[BankAccountInfo] (UserID,[BankName],[AccountNumber]) VALUES (@UserID,@BankName,@AccountNumber)

GO

--17.12.2016
create procedure Info.SaveCardInformation(
@UserID int,@cardNumber nvarchar(16),
@expireDate nvarchar(8),@cvv int)	

as
INSERT INTO [Info].[CardInfo] (UserID,[cardNumber],
[expireDate],[cvv]) VALUES (@UserID,@cardNumber ,
@expireDate,@cvv)

GO

  INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('CURRENT')
 INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('SEND')
  INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('RECEIVE')
   INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('SUSPEND')
   GO
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,1,1,1,GETDATE())
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,1,1,2,GETDATE())
 INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,1,1,3,GETDATE())


   GO
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,2,2,1,GETDATE())
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,2,2,2,GETDATE())
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,2,2,3,GETDATE())
   GO
      INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,3,3,1,GETDATE())
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,3,3,2,GETDATE())
 INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,3,3,3,GETDATE())



   --
  

  INSERT INTO [Info].[InformationIs]([UserID],[IsMail],[IsPhone],[InfoContext])VALUES(1,1,1,'MERHABA1')
  INSERT INTO [Info].[InformationIs]([UserID],[IsMail],[IsPhone],[InfoContext])VALUES(2,1,1,'MERHABA2')
  GO

--UserID,[BankName],[AccountNumber],[bin],[cardNumber],[expireDate],[cvv]



go
SELECT * FROM [Info].[Currency]
GO
 select * from [Info].[Users]
 go

 select * from Info.TimeZones
 go
 SELECT * FROM [Info].[TransactionType]
 GO

  SELECT * FROM [Info].[Transaction]
  GO
 

  /*select C.[FirstName],C.Email,T.Balance,TT.TransactionType from [Info].[Users] as C join  [Info].[Transaction] as T ON C.UserID=T.UserID JOIN [Info].[TransactionType] AS TT ON T.TransactionTypeID=TT.TransactionTypeID
  GO*/


SELECT * FROM [Info].[Transaction]
GO 

create procedure Info.SendMoney(@Balance as DECIMAL(12,6),@TransactionType as int,
@SenderID as int,@ReceiverID as int,@CurrencyID as int)
as

INSERT INTO [Info].[Transaction]([Balance],[TransactionTypeID],[SenderID],[ReceiverID],[CurrencyID],TransactionTime)
VALUES(@Balance,@TransactionType,@SenderID,@ReceiverID,@CurrencyID,GETDATE())
          
GO


--SEND REC-SEND-ssn,transaction id to receipt page,create receipt number,user write description,
create procedure Info.SaveReceiptInfo(
	@receiverID int ,
	@senderID int ,
	@receiptNumber nvarchar(12)  ,
	@receiptDate datetime ,
	@receiptDescription nvarchar(100) ,
	@SSN int,
	@TransactionID	int
)
AS
INSERT INTO [Info].[Receipt]([receiverID],[senderID],[receiptNumber],[receiptDate],[receiptDescription],
[SSN],[TransactionID])
VALUES(@receiverID  ,
	@senderID  ,
	@receiptNumber   ,
	@receiptDate  ,
	@receiptDescription ,
	@SSN ,
	@TransactionID)
GO

create procedure Info.addMoneyToUser(@Balance as DECIMAL(12,6),@UserID as int)
as
update  [Info].[Transaction] set [Balance] = @Balance,[TransactionTime]=GETDATE() where [TransactionTypeID]=1 and [ReceiverID]=@UserID and [SenderID]=@UserID and [CurrencyID]=1
GO

create procedure Info.addMoneyFromCard(@Balance as DECIMAL(12,6),@UserID as int,@CurrencyID as int)
as
update  [Info].[Transaction] set [Balance] = @Balance+[Balance],[TransactionTime]=GETDATE() where [TransactionTypeID]=1 and [ReceiverID]=@UserID and [SenderID]=@UserID and [CurrencyID]=@CurrencyID
GO






select U.UserID,T.Balance,TT.TransactionType from [Info].[Users] as U join [Info].[Transaction] as T ON U.UserID=T.ReceiverID 
join [Info].[TransactionType] as TT ON TT.TransactionTypeID=T.TransactionTypeID
GO


	/*
create trigger Info.UpdateCurrent on [Info].[Transaction]
after insert as
begin

	Declare @SenderID int
	Declare @ReceiverID int
	Declare @Balance decimal
	Declare @TransactionTypeID int
	Declare @CurrencyID int

	select @SenderID = inserted.SenderID, @ReceiverID = inserted.ReceiverID,@Balance=inserted.Balance,@TransactionTypeID=inserted.TransactionTypeID,@CurrencyID=inserted.CurrencyID from inserted

	begin
		UPDATE [Info].[Transaction] set [Balance]=@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.SenderID=@SenderID  AND T.TransactionTypeID=@TransactionTypeID AND T.CurrencyID=@CurrencyID

	UPDATE [Info].[Transaction]  set [Balance]=Balance-@Balance,TransactionTime=GETDATE() from [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE T.SenderID=@SenderID AND T.TransactionTypeID=1 AND T.CurrencyID=@CurrencyID
	
	UPDATE [Info].[Transaction] set [Balance]=@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.ReceiverID=@ReceiverID  AND T.CurrencyID=@CurrencyID AND T.TransactionTypeID=@TransactionTypeID
	
	UPDATE [Info].[Transaction] set [Balance]=[Balance]+@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.ReceiverID=@ReceiverID  AND T.TransactionTypeID=1  AND T.CurrencyID=@CurrencyID
	end

end

go*/


--asıl trigger

create trigger Info.InsertCurrent on [Info].[Transaction]
after insert as
begin

	Declare @SenderID int
	Declare @ReceiverID int
	Declare @Balance decimal
	Declare @TransactionTypeID int
	Declare @CurrencyID int
	Declare @TransactionID int

	select @TransactionID=inserted.TransactionID, @SenderID = inserted.SenderID, @ReceiverID = inserted.ReceiverID,@Balance=inserted.Balance,@TransactionTypeID=inserted.TransactionTypeID,@CurrencyID=inserted.CurrencyID from inserted

	begin
		
	 INSERT INTO [Info].[DoneTransaction] ([TransactionTime],[TransactionError],[TransactionAmount],[TransactionID])VALUES(getdate(),'success',@Balance,@TransactionID)
	 INSERT INTO [Info].[DoneTransactionType]([TransactionID],[UserID],[DoneTransactionType])VALUES(@TransactionID,@SenderID,'SENDED')
	 INSERT INTO [Info].[DoneTransactionType]([TransactionID],[UserID],[DoneTransactionType])VALUES(@TransactionID,@ReceiverID,'RECEIVED')
	UPDATE [Info].[Transaction]  set [Balance]=Balance-@Balance,TransactionTime=GETDATE() from [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE T.SenderID=@SenderID AND T.TransactionTypeID=1 AND T.CurrencyID=@CurrencyID
		
	
	UPDATE [Info].[Transaction] set [Balance]=[Balance]+@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.ReceiverID=@ReceiverID  AND T.TransactionTypeID=1  AND T.CurrencyID=@CurrencyID
	end

end
go


create procedure Info.ShowBalanceByCurrencyType(@receiverID AS int,@CurrencyType as nvarchar(10))
as
SELECT T.Balance,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE  T.ReceiverID=@receiverID AND T.TransactionTypeID=1 AND C.CurrencyType=@CurrencyType
GO--t.senderID kontrolünü yap

create procedure Info.ShowBalanceByUserID(@receiverID AS int)
as
SELECT T.Balance,C.CurrencyType FROM [Info].[Transaction] AS T  join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE  T.ReceiverID=@receiverID AND T.TransactionTypeID=1
GO

exec Info.ShowBalanceByCurrencyType 1,'TR'
go
exec Info.ShowBalanceByUserID 1
go
SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE  T.ReceiverID=1 AND T.TransactionTypeID=1
go
--SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=@CurrencyID AND T.ReceiverID=@receiverID AND T.TransactionTypeID=1


Insert into [Info].[SecurityQuest]([UserID],[SecurityQuestion],[SecurityAnswer])VALUES(1,'SS1','SS1')
Insert into [Info].[SecurityQuest]([UserID],[SecurityQuestion],[SecurityAnswer])VALUES(2,'SS2','SS2')
go



exec Info.addMoneyToUser 200.000000,1
go


exec Info.addMoneyToUser 200.000000,2
go
exec Info.addMoneyFromCard 100.000000,1,1
go
exec Info.addMoneyFromCard 100.000000,1,2
go
exec Info.addMoneyFromCard 100.000000,1,3
go
exec Info.addMoneyFromCard 100.000000,2,1
go
exec Info.addMoneyFromCard 100.000000,2,2
go
exec Info.addMoneyFromCard 100.000000,2,3
go
exec Info.ShowBalanceByUserID 1
go
exec Info.ShowBalanceByUserID 2
go



select * from [Info].[Transaction]
go
select * from [Info].[Users]
go
select * from [Info].[Transaction] AS T WHERE T.CurrencyID=1
GO
select * from [Info].[Transaction] AS T WHERE T.CurrencyID=2
GO
select * from [Info].[Transaction] AS T WHERE T.CurrencyID=3
go

--exec Info.SendMoney 12,2,1,2,1
go
--exec Info.SendMoney 12,2,2,1,1
go

--exec Info.SendMoney 10,2,1,2,2
go

USE PayMeDB
--@Balance as DECIMAL(12,6),@TransactionType as int,@SenderID as int,@ReceiverID as int,@CurrencyID as int)
go
/*
EXEC Info.SendMoney 10,2,1,2,1
go
EXEC Info.SendMoney 10,2,2,1,1
go
EXEC Info.SendMoney 10,2,1,2,3
go
exec Info.SendMoney 10,3,2,1,2
go
exec Info.SendMoney 10,3,2,1,2
go
exec Info.SendMoney 10,3,2,1,3
go
exec Info.SendMoney 10,3,1,2,1
go

exec Info.SendMoney 10,3,2,1,3
go

exec Info.SendMoney 10,3,1,2,3
go
*/

SELECT *,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
GO

SELECT *,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=2
GO

SELECT *,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=1 AND T.ReceiverID=1 AND T.TransactionTypeID=1
GO

SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=3 AND T.ReceiverID=1 AND T.TransactionTypeID=1
GO
USE PayMeDB

select * from Info.Users

select * from Info.TimeZones

select * from Info.SecurityQuest


select * from [Info].[Transaction]
go

select Balance from [Info].[Transaction] where ReceiverID=1  AND CurrencyID=1
GO
select * from [Info].[Receipt]
go
select * from [Info].[DoneTransaction]
go



select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime) AS Time from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID WHERE T.TransactionTime > '2016.11.16' AND T.TransactionTime < '2016.12.17'
GO


exec Info.ShowBalanceByUserID 1
go
exec Info.ShowBalanceByUserID 2
go

--exec Info.SendMoney 10,2,1,2,1
go
--exec Info.SendMoney 30,2,1,2,1
go

--exec Info.SendMoney 20,2,2,1,1
go 
--exec Info.SendMoney 40,2,2,1,1
go 


select * from [Info].[Transaction]
go
select * from ınfo.DoneTransaction

select DTT.UserID,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID  where DTT.UserID=1
GO
select D.TransactionAmount,TT.TransactionType,T.TransactionTime,T.ReceiverID,T.SenderID from [Info].[DoneTransaction] as D 
join   [Info].[Transaction] as T ON D.TransactionID=T.TransactionID JOIN [Info].[TransactionType] AS TT ON T.TransactionTypeID= TT.TransactionTypeID
go

create procedure Info.ShowActivityBtwDateByID(@StartDate as datetime,@FinishDate as datetime,@UserID as int)
as 
select DTT.DoneTransactionTypeID,U.FirstName,U.LastName, DTT.UserID,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,C.CurrencyType from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=@UserID AND DT.TransactionTime > @StartDate AND DT.TransactionTime < @FinishDate
ORDER BY DT.TransactionTime DESC
GO


EXEC INFO.ShowActivityBtwDateByID '2016-11-11','2016-12-18',1
GO
EXEC INFO.ShowActivityBtwDateByID '2016-11-11','2016-12-18',2
GO


create procedure Info.ShowActivityByTransType(@TransactionType as nvarchar(20),@UserID as int)
as 
select DTT.DoneTransactionTypeID, U.FirstName,U.LastName, DTT.UserID,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,C.CurrencyType from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=@UserID AND DTT.DoneTransactionType=@TransactionType
ORDER BY DT.TransactionTime DESC
GO


create procedure Info.ShowActivityByUserID(@UserID as int)
as 
select DTT.DoneTransactionTypeID, U.FirstName,U.LastName, DTT.UserID,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,C.CurrencyType from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=@UserID
ORDER BY DT.TransactionTime DESC
GO

EXEC Info.ShowActivityByUserID 1
go

EXEC INFO.ShowActivityByTransType 'SENDED',1
GO
EXEC INFO.ShowActivityByTransType 'RECEIVED',1
GO
EXEC INFO.ShowActivityByTransType 'SENDED',2
GO
EXEC INFO.ShowActivityByTransType 'RECEIVED',2
GO

select DTT.DoneTransactionTypeID,U.FirstName,U.LastName, DTT.UserID,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,C.CurrencyType from 
[Info].[DoneTransactionType] AS DTT JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction]
 AS T ON T.TransactionID=DT.TransactionID JOIN [Info].[Users] 
AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=2 AND DT.TransactionTime > '2016-11-11' AND DT.TransactionTime < '2016-12-18'
ORDER BY DT.TransactionTime DESC
GO

create procedure Info.ShowActivityByUserName(@UserName as nvarchar(20),@UserSurname as nvarchar(20),@UserID as int)
as 
select DTT.DoneTransactionTypeID,U.FirstName,U.LastName, DTT.UserID,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,C.CurrencyType from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=@UserID AND 
((U.FirstName like '%'+@UserName+'%' OR U.LastName like '%'+@UserSurname+'%') OR (U.FirstName like '%'+@UserName+'%' OR U.LastName like '%'+@UserName+'%' ) 
OR (U.FirstName like '%'+@UserSurname+'%' OR U.LastName like '%'+@UserSurname+'%' ) OR (U.FirstName like '%'+@UserSurname+'%') OR (U.LastName like '%'+@UserName+'%' ))
ORDER BY DT.TransactionTime DESC
GO

EXEC INFO.ShowActivityByUserName 'onur',' ',1
go

create procedure Info.ShowUserInfo(@UserID as int)
as
select * from [Info].[Users] where [UserID]=@UserID
go

create procedure Info.UpdateUserProfile(
@UserID AS int,
@FirstName as nvarchar(30),
@LastName as nvarchar(30),
@SSN as int,
@Password as nvarchar(20),
@Email as nvarchar(50),
@Address as nvarchar(100),
@Country as nvarchar(25),
@BirthDate as [datetime],
@PicturePath as nvarchar(150),
@PhoneNumber as nvarchar(20)
)
as
Update [Info].[Users] set 
	FirstName=@FirstName ,
	LastName=@LastName ,
	SSN =@SSN,
	[Password] =@Password,
	[Email]=@Email ,
	[Address] =@Address,
	[Country]=@Country ,
	[BirthDate]=@BirthDate ,
	[PicturePath]=@PicturePath ,--Profil picture url
	PhoneNumber =@PhoneNumber where UserID=@UserID
go


exec Info.UpdateUserProfile 1,'Masum','Olgun','11111111','PWD','ema1@gmail.com','ADRESS','CNTY','1990-01-01','mypicture','21312312312'

GO
SELECT * FROM [Info].[Users] 
GO
exec Info.SaveBankInformation 1,'iş bankası','2132'
go
select *  from [Info].[BankAccountInfo]
go

create procedure Info.UpdateBankAccountInfo(
@BankAccountInfoID AS int,
@UserID int,
@BankName nvarchar(50),
@AccountNumber nvarchar(30)
)
as
Update [Info].[BankAccountInfo] set  [BankName]=@BankName,[AccountNumber]=@AccountNumber where [UserID]=@UserID and BankAccountInfoID=@BankAccountInfoID
go

create procedure Info.DeleteBankAccountInfo(@UserID int,
@BankAccountInfoID as int
)
as
delete from [Info].[BankAccountInfo]  where [UserID]=@UserID AND BankAccountInfoID=@BankAccountInfoID
go


create procedure Info.DisplayBankInfoByBib(
@UserID int,
@BankAccountInfoID int
)
as
select * from [Info].[BankAccountInfo] as B where B.UserID=@UserID and B.BankAccountInfoID=@BankAccountInfoID
go


create procedure Info.DisplayBankInfo(
@UserID int
)
as
select * from [Info].[BankAccountInfo] as B where B.UserID=@UserID
go

EXEC Info.DisplayBankInfo 1
go

exec Info.UpdateBankAccountInfo 2,2,'aaaa','111'
go
exec Info.DeleteBankAccountInfo 2,2
GO


exec Info.SaveCardInformation 1,'123','11-2016','111'
go

create procedure Info.UpdateCardInfo(
@CardInfoID int, 
@UserID int,
@cardNumber nvarchar(16),
@expireDate nvarchar(8),
@cvv int 
)
as
Update [Info].[CardInfo] set [cardNumber]= @cardNumber,[expireDate]=@expireDate,[cvv]=@cvv where [UserID]=@UserID and CardInfoID=@CardInfoID
go


create procedure Info.DeleteCardInfo(
@CardInfoID int,
@UserID int

)
as
delete from [Info].[CardInfo] where [UserID]=@UserID AND CardInfoID=@CardInfoID
go

select * from [Info].[CardInfo]
go

exec Info.UpdateCardInfo 7,1,'1234','11-1111',2222
go
---OLUŞMADI
create procedure Info.DisplayCardInfo(
@UserID int
)
as
select * from [Info].[CardInfo] as C where C.UserID=@UserID
go

create procedure Info.DisplayCardInfoByCid(
@UserID int,
@CardInfoID int

)
as
select * from [Info].[CardInfo] as C where C.UserID=@UserID and CardInfoID=@CardInfoID
go

Exec Info.DisplayCardInfo 1
go

select * from [Info].[Users]
go
--GÖNDERENİN  fatura Bilgileri + alanın fatura bilgileri,bir arada çalışacak
select U.FirstName,U.LastName,U.SSN,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,DTT.DoneTransactionTypeID from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID where DTT.UserID=1 AND DTT.DoneTransactionTypeID=1
ORDER BY DT.TransactionTime DESC
select U.FirstName,U.LastName,U.SSN,DTT.DoneTransactionTypeID from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.ReceiverID where DTT.UserID=1 AND DTT.DoneTransactionTypeID=1
ORDER BY DT.TransactionTime DESC
GO
--

create procedure Info.ShowInvoice(@UserID as int,@DoneTransactionTypeID as int)
as 
select U.FirstName,U.LastName,U.SSN,DTT.DoneTransactionType,DT.TransactionTime,DT.TransactionAmount,DTT.DoneTransactionTypeID,C.CurrencyType from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=@UserID AND DTT.DoneTransactionTypeID=@DoneTransactionTypeID
ORDER BY DT.TransactionTime DESC
select U.FirstName,U.LastName,U.SSN,DTT.DoneTransactionTypeID from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.ReceiverID where DTT.UserID=@UserID AND DTT.DoneTransactionTypeID=@DoneTransactionTypeID
ORDER BY DT.TransactionTime DESC
GO

SELECT * FROM [Info].[DoneTransactionType]
GO

EXEC Info.ShowInvoice 1,1
go
EXEC Info.ShowInvoice 1,4
go

EXEC Info.ShowInvoice 2,2
go



go




create procedure Info.CreateInvoice(
@UserID as int,
@DoneTransactionTypeID as int ,
@InvoiceControl as nvarchar(15)
)
as 

DECLARE @SenderFirstName as nvarchar(30) 
DECLARE @SenderLastName as nvarchar(30) 
DECLARE @SenderSSN as int 
DECLARE  @ReceiverFirstName as nvarchar(30) 
DECLARE  @ReceiverLastName as nvarchar(30) 
DECLARE @ReceiverSSN as int 
DECLARE @DoneTransactionType as nvarchar(30)  
DECLARE @TransactionAmount as decimal(12,6) 
DECLARE @TransactionTime as datetime
DECLARE @CurrencyType as char(5)

select @SenderFirstName= U.FirstName,@SenderLastName=U.LastName,@SenderSSN=U.SSN,@TransactionTime=DT.TransactionTime,
@TransactionAmount=DT.TransactionAmount,@DoneTransactionTypeID=DTT.DoneTransactionTypeID,@DoneTransactionType=DTT.DoneTransactionType, @CurrencyType=C.CurrencyType from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.SenderID JOIN [Info].[Currency] AS C ON C.CurrencyID=T.CurrencyID where DTT.UserID=@UserID AND DTT.DoneTransactionTypeID=@DoneTransactionTypeID
ORDER BY DT.TransactionTime DESC
select @ReceiverFirstName=U.FirstName,@ReceiverLastName=U.LastName,@ReceiverSSN=U.SSN from [Info].[DoneTransactionType] AS DTT 
JOIN [Info].[DoneTransaction] as DT ON DTT.TransactionID =DT.TransactionID JOIN [Info].[Transaction] AS T ON T.TransactionID=DT.TransactionID 
JOIN [Info].[Users] AS U ON U.UserID=T.ReceiverID where DTT.UserID=@UserID AND DTT.DoneTransactionTypeID=@DoneTransactionTypeID
ORDER BY DT.TransactionTime DESC

insert into [Info].[Invoice] ([DoneTransactionTypeID],[UserID],[SenderFirstName],[SenderLastName],[SenderSSN],[ReceiverFirstName],[ReceiverLastName],[ReceiverSSN],[DoneTransactionType],[TransactionAmount],[TransactionTime],InvoiceControl,[CurrencyType])
VALUES(@DoneTransactionTypeID,@UserID,@SenderFirstName,@SenderLastName,@SenderSSN,@ReceiverFirstName,@ReceiverLastName,@ReceiverSSN,@DoneTransactionType,@TransactionAmount,@TransactionTime,@InvoiceControl,@CurrencyType)

go

--EXEC Info.CreateInvoice 1,1,'ıc123'
go

--EXEC Info.ShowInvoice 1,1
go

create procedure Info.DisplayInvoiceByControl(
@InvoiceControl as nvarchar(15)
)
as 
select * from [Info].[Invoice] where InvoiceControl=@InvoiceControl

go

--exec Info.DisplayInvoiceByControl 'ıc123'
go

create procedure Info.DisplayInvoice(
@UserID  as int,
@DoneTransactionTypeID as int 
)
as 
select * from [Info].[Invoice] where DoneTransactionTypeID=@DoneTransactionTypeID and UserID=@UserID
go


select * from [Info].[Invoice]
go

select* from Info.Users
go

create procedure Info.newUserBalance(
@UserID  as int
)
as 
INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,@UserID,@UserID,1,GETDATE())
INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,@UserID,@UserID,2,GETDATE())
INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,@UserID,@UserID,3,GETDATE())
go

select * from [Info].[Users]
go

select * from [Info].[Transaction]
go

create procedure Info.closeUserAccount(@userID as int)
as
update [Info].[Users] set isActive=0 where [UserID]=@userID
go

--exec Info.closeUserAccount 3
create procedure Info.SelectUserImage(@UserID as int)
as
Select PicturePath from Info.Users where UserID = @UserID;
go

exec Info.SelectUserImage 1
