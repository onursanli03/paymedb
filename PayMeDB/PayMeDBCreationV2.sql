﻿use master
go


create database PayMeDB

go
use  PayMeDB
go

create schema Info

go




create table Info.[Users](

	[UserID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	FirstName nvarchar(30) not null,
	LastName nvarchar(30) not null,
	SSN int not null unique,
	[Password] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](50) NOT NULL unique,
	[Address] nvarchar(100) null,
	[Country] nvarchar(25) not null,
	[ActivationCode] [int] NOT NULL,
	[isActive] [bit] NOT NULL default 1,--user is active or not,
	[BirthDate] [datetime]  NULL,
	[PicturePath] [nvarchar](150) NULL,--Profil picture url
	[RegisterDate] [datetime] NULL,
	PhoneNumber nvarchar  null)

	Create table [Info].[TimeZones](
	UserID	int not null foreign key references Info.Users(UserID),
	[TimeZoneID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[TimeZoneValue] [nvarchar](100) Not Null unique
)

create table Info.InformationIs(
	
	UserID int not null foreign key references Info.Users(UserID),
	IsMail bit not null default 0,
	IsPhone bit not null default 0,
	InfoContext nvarchar(100) not null /*mail and phone context for security*/
)

create table Info.TransactionType(
	
	TransactionTypeID int not null primary key identity(1,1),
	TransactionType nvarchar(30) not null
)

create table Info.Currency(

	CurrencyID int not null primary key identity(1,1),
	CurrencyType char(5) not null,	
	ForeignCurrencyAmount decimal(12,6) not null
)

create table Info.[Transaction](

	TransactionID int not null primary key IDENTITY(1,1),
	Balance decimal(12,6) not null,
	TransactionTypeID int foreign key references Info.TransactionType(TransactionTypeID),
	ReceiverID int NOT NULL foreign key references [Info].[Users]([UserID]),
	SenderID int NOT NULL foreign key references [Info].[Users]([UserID]),
	CurrencyID int foreign key references [Info].[Currency](CurrencyID),
	TransactionTime datetime not null

)





create table Info.DoneTransaction(
	DoneTransactionID int primary key identity(1,1),
	TransactionID int foreign key references Info.[Transaction](TransactionID),
	TransactionTime datetime,
	TransactionError nvarchar(100) null,
	TransactionAmount decimal(12,6) not null
)

CREATE TABLE Info.binlist (
  bin int NOT NULL primary key identity(1,1),
  banka_kodu int DEFAULT NULL,
  banka_adi varchar(100) DEFAULT NULL,
  [type] varchar(50) DEFAULT NULL,
  sub_type varchar(50) DEFAULT NULL,
  virtual varchar(50) DEFAULT NULL,
  prepaid varchar(50) DEFAULT NULL

)  

create table Info.BankInformation(
	UserID int not null foreign key references Info.Users(UserID),
	BankName nvarchar(50) null,
	AccountNumber nvarchar(30),
	bin int foreign key references Info.binlist(bin),
	cardNumber nvarchar(16) null,
	[expireDate] datetime null,
	cvv int null
)

create table Info.Receipt(
	receiptID int not null identity(1,1) primary key,
	receiverID int not null foreign key references Info.Users(UserID),
	senderID int not null foreign key references Info.Users(UserID),
	receiptNumber nvarchar(12) not null ,
	receiptDate datetime not null,
	receiptDescription nvarchar(100) not null,
	SSN int references Info.Users(SSN),
	[TransactionID]	int not null foreign key references [Info].[Transaction]([TransactionID])
)

GO

create table Info.SecurityQuest(
	SecurityQuest int not null primary key identity(1,1),
	UserID int not null foreign key references Info.Users(UserID),
	SecurityQuestion nvarchar(100) not null,
	SecurityAnswer nvarchar(30) not null
)
go

create table Info.ContactUs(
ContactUs int not null primary key identity(1,1),
[Name] nvarchar(50) null,
[Email] nvarchar(50) null,
PhoneNumber nvarchar(30)  null,
[Message] nvarchar(500) null
)
go

create procedure Info.SaveContactUs(
@Name nvarchar(50) ,
@Email nvarchar(50) ,
@PhoneNumber nvarchar(30)  ,
@Message nvarchar(500)
)
as INSERT INTO [Info].[ContactUs]([Name],[Email],[PhoneNumber],[Message])
VALUES(@Name,@Email,@PhoneNumber,@Message)
GO




--register user
create procedure Info.RegisterUser(
@FirstName nvarchar(30),
@LastName nvarchar(30),
@SSN int,
@Password [nvarchar](20),
@Email [nvarchar](50),
@Address nvarchar(100),
@Country nvarchar(25),
@BirthDate [datetime],
@TimeZone nvarchar(100),
@SecurityQ nvarchar(100),
@SecurityAns nvarchar(30)
)
 as
INSERT INTO Info.[Users]
 (FirstName,
LastName,
SSN,
[Password],
Email,
[Address],
[Country],
ActivationCode,
isActive,
BirthDate,
PicturePath,
RegisterDate,
PhoneNumber
)
     VALUES
(@FirstName,
@LastName,
@SSN,
@Password,
@Email,
@Address,
@Country,
0,
0,
@BirthDate,
null,
GETDATE(),
null
)
declare @userId int
select @userId=SCOPE_IDENTITY();
INSERT INTO Info.TimeZones (UserID,TimeZoneValue) values(@userId,@TimeZone)
INSERT INTO  [Info].[SecurityQuest](UserID,[SecurityQuestion],SecurityAnswer) values(@userId,@SecurityQ,@SecurityAns)

GO


create procedure Info.SaveBinInfo(
  @banka_kodu int ,
  @banka_adi varchar(100),
  @type varchar(50) ,
  @sub_type varchar(50),
  @virtual varchar(50) ,
  @prepaid varchar(50)
)
as INSERT INTO [Info].[binlist]([banka_kodu],[banka_adi],[type],[sub_type],[virtual],[prepaid])
VALUES(@banka_kodu,
  @banka_adi,
  @type,
  @sub_type,
  @virtual ,
  @prepaid)
GO



create procedure Info.SaveBankInformation(
@UserID int,@BankName nvarchar(50),@AccountNumber nvarchar(30),@bin int,@cardNumber nvarchar(16),
@expireDate datetime,@cvv int)

as
INSERT INTO [Info].[BankInformation] (UserID,[BankName],[AccountNumber],[bin],[cardNumber],
[expireDate],[cvv]) VALUES (@UserID,@BankName,@AccountNumber,@bin ,@cardNumber ,
@expireDate,@cvv)

GO




go
 --register users
 exec Info.RegisterUser 'MASUM','OLGUN',123,'PWD','ema1@gmail.com','ADRS1','country1',0,1111,1,'s'
 exec Info.RegisterUser 'ONUR','SANLI',124,'PWD2','ema2@gmail.com','ADRS2','country2',0,2222,1,'s'
 go
 --insert to currency
 INSERT INTO  [Info].Currency([CurrencyType],ForeignCurrencyAmount)VALUES('TR',1)
 INSERT INTO  [Info].Currency([CurrencyType],ForeignCurrencyAmount)VALUES('USD',2)
 INSERT INTO  [Info].Currency([CurrencyType],ForeignCurrencyAmount)VALUES('EU',3)
 
 GO



  INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('CURRENT')
 INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('SEND')
  INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('RECEIVE')
   INSERT INTO [Info].[TransactionType]([TransactionType])VALUES('SUSPEND')
   GO
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,1,1,1,GETDATE())
    INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,1,1,2,GETDATE())
 INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,1,1,3,GETDATE())


   GO
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,2,2,1,GETDATE())
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,2,2,2,GETDATE())
   INSERT INTO  [Info].[Transaction]([Balance],[TransactionTypeID],ReceiverID,SenderID,CurrencyID,TransactionTime)VALUES(0,1,2,2,3,GETDATE())
   GO

   --[banka_kodu],[banka_adi],[type],[sub_type],[virtual],[prepaid]
   exec Info.SaveBinInfo 1,'banka1','type1','subtype1','virt1','prepaid1'
   exec Info.SaveBinInfo 2,'banka1','type1','subtype1','virt1','prepaid1'
   go

   --
  --INSERT INTO [Info].[BankInformation]([BankName],[AccountNumber],[bin],[cardNumber],[expireDate],[cvv]) VALUES (1,'bank11',11,1,123,null,111) 
  --INSERT INTO [Info].[BankInformation]([BankName],[AccountNumber],[bin],[cardNumber],[expireDate],[cvv])VALUES(2,'bank11',11,2,123,null,111)
  go

  INSERT INTO [Info].[InformationIs]([UserID],[IsMail],[IsPhone],[InfoContext])VALUES(1,1,1,'MERHABA1')
  INSERT INTO [Info].[InformationIs]([UserID],[IsMail],[IsPhone],[InfoContext])VALUES(2,1,1,'MERHABA2')
  GO

--UserID,[BankName],[AccountNumber],[bin],[cardNumber],[expireDate],[cvv]

exec Info.SaveBankInformation 1,'bb','ads',1,'cc','2016-11-11',123
exec Info.SaveBankInformation 2,'bb2','ads2',2,'cc2','2016-10-10',1234
go


go
SELECT * FROM [Info].[Currency]
GO
 select * from [Info].[Users]
 go

 select * from Info.TimeZones
 go
 SELECT * FROM [Info].[TransactionType]
 GO

  SELECT * FROM [Info].[Transaction]
  GO
  select * from [Info].[binlist]
  go
  select * from [Info].[BankInformation]
  go

  /*select C.[FirstName],C.Email,T.Balance,TT.TransactionType from [Info].[Users] as C join  [Info].[Transaction] as T ON C.UserID=T.UserID JOIN [Info].[TransactionType] AS TT ON T.TransactionTypeID=TT.TransactionTypeID
  GO*/


SELECT * FROM [Info].[Transaction]
GO 

create procedure Info.SendMoney(@Balance as DECIMAL(12,6),@TransactionType as int,
@SenderID as int,@ReceiverID as int,@CurrencyID as int)
as

INSERT INTO [Info].[Transaction]([Balance],[TransactionTypeID],[SenderID],[ReceiverID],[CurrencyID],TransactionTime)
VALUES(@Balance,@TransactionType,@SenderID,@ReceiverID,@CurrencyID,GETDATE())
          
GO


--SEND REC-SEND-ssn,transaction id to receipt page,create receipt number,user write description,
create procedure Info.SaveReceiptInfo(
	@receiverID int ,
	@senderID int ,
	@receiptNumber nvarchar(12)  ,
	@receiptDate datetime ,
	@receiptDescription nvarchar(100) ,
	@SSN int,
	@TransactionID	int
)
AS
INSERT INTO [Info].[Receipt]([receiverID],[senderID],[receiptNumber],[receiptDate],[receiptDescription],
[SSN],[TransactionID])
VALUES(@receiverID  ,
	@senderID  ,
	@receiptNumber   ,
	@receiptDate  ,
	@receiptDescription ,
	@SSN ,
	@TransactionID)
GO

create procedure Info.addMoneyToUser(@Balance as DECIMAL(12,6),@UserID as int)
as
update  [Info].[Transaction] set [Balance] = @Balance,[TransactionTime]=GETDATE() where [TransactionTypeID]=1 and [ReceiverID]=@UserID and [SenderID]=@UserID and [CurrencyID]=1
GO

create procedure Info.addMoneyFromCard(@Balance as DECIMAL(12,6),@UserID as int,@CurrencyID as int)
as
update  [Info].[Transaction] set [Balance] = @Balance+[Balance],[TransactionTime]=GETDATE() where [TransactionTypeID]=1 and [ReceiverID]=@UserID and [SenderID]=@UserID and [CurrencyID]=@CurrencyID
GO



/*
create procedure Info.ConvertCurrency(@Balance as DECIMAL(8,6),@TransactionType as int,@ReceiverID as int,@CurrencyID1 as int,@CurrencyID2 as int)
as
select  T.Balance,C.CurrencyType,T.ReceiverID From [Info].[Transaction] as T join [Info].[TransactionType] as TT ON TT.TransactionTypeID=T.TransactionTypeID 
join [Info].[Currency] as C on C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=3 AND T.ReceiverID=1 AND T.SenderID=1       
GO
*/


select U.UserID,T.Balance,TT.TransactionType from [Info].[Users] as U join [Info].[Transaction] as T ON U.UserID=T.ReceiverID 
join [Info].[TransactionType] as TT ON TT.TransactionTypeID=T.TransactionTypeID
GO


	/*
create trigger Info.UpdateCurrent on [Info].[Transaction]
after insert as
begin

	Declare @SenderID int
	Declare @ReceiverID int
	Declare @Balance decimal
	Declare @TransactionTypeID int
	Declare @CurrencyID int

	select @SenderID = inserted.SenderID, @ReceiverID = inserted.ReceiverID,@Balance=inserted.Balance,@TransactionTypeID=inserted.TransactionTypeID,@CurrencyID=inserted.CurrencyID from inserted

	begin
		UPDATE [Info].[Transaction] set [Balance]=@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.SenderID=@SenderID  AND T.TransactionTypeID=@TransactionTypeID AND T.CurrencyID=@CurrencyID

	UPDATE [Info].[Transaction]  set [Balance]=Balance-@Balance,TransactionTime=GETDATE() from [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE T.SenderID=@SenderID AND T.TransactionTypeID=1 AND T.CurrencyID=@CurrencyID
	
	UPDATE [Info].[Transaction] set [Balance]=@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.ReceiverID=@ReceiverID  AND T.CurrencyID=@CurrencyID AND T.TransactionTypeID=@TransactionTypeID
	
	UPDATE [Info].[Transaction] set [Balance]=[Balance]+@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.ReceiverID=@ReceiverID  AND T.TransactionTypeID=1  AND T.CurrencyID=@CurrencyID
	end

end

go*/

create trigger Info.InsertCurrent on [Info].[Transaction]
after insert as
begin

	Declare @SenderID int
	Declare @ReceiverID int
	Declare @Balance decimal
	Declare @TransactionTypeID int
	Declare @CurrencyID int
	Declare @TransactionID int

	select @TransactionID=inserted.TransactionID, @SenderID = inserted.SenderID, @ReceiverID = inserted.ReceiverID,@Balance=inserted.Balance,@TransactionTypeID=inserted.TransactionTypeID,@CurrencyID=inserted.CurrencyID from inserted

	begin
		
	 INSERT INTO [Info].[DoneTransaction] ([TransactionTime],[TransactionError],[TransactionAmount],[TransactionID])VALUES(getdate(),'success',@Balance,@TransactionID)

	UPDATE [Info].[Transaction]  set [Balance]=Balance-@Balance,TransactionTime=GETDATE() from [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID 
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE T.SenderID=@SenderID AND T.TransactionTypeID=1 AND T.CurrencyID=@CurrencyID
		
	
	UPDATE [Info].[Transaction] set [Balance]=[Balance]+@Balance,TransactionTime=GETDATE() FROM [Info].[Transaction] as T  join [Info].[TransactionType] AS TT ON TT.TransactionTypeID=T.TransactionTypeID
	join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
	 WHERE T.ReceiverID=@ReceiverID  AND T.TransactionTypeID=1  AND T.CurrencyID=@CurrencyID
	end

end

go
create procedure Info.ShowBalanceByCurrencyType(@receiverID AS int,@CurrencyType as nvarchar(10))
as
SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE  T.ReceiverID=@receiverID AND T.TransactionTypeID=1 AND C.CurrencyType=@CurrencyType
GO--t.senderID kontrolünü yap

create procedure Info.ShowBalanceByUserID(@receiverID AS int)
as
SELECT T.Balance FROM [Info].[Transaction] AS T  join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE  T.ReceiverID=@receiverID AND T.TransactionTypeID=1
GO

exec Info.ShowBalanceByCurrencyType 1,'TR'
go
exec Info.ShowBalanceByUserID 1
go
SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE  T.ReceiverID=1 AND T.TransactionTypeID=1
go
--SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=@CurrencyID AND T.ReceiverID=@receiverID AND T.TransactionTypeID=1


Insert into [Info].[SecurityQuest]([UserID],[SecurityQuestion],[SecurityAnswer])VALUES(1,'SS1','SS1')
Insert into [Info].[SecurityQuest]([UserID],[SecurityQuestion],[SecurityAnswer])VALUES(2,'SS2','SS2')
go


exec Info.addMoneyToUser 100.000000,1
go

exec Info.addMoneyToUser 100.000000,2
go
exec Info.addMoneyFromCard 50.000000,1,1
go
exec Info.addMoneyFromCard 50.000000,1,2
go
exec Info.addMoneyFromCard 50.000000,1,3
go
exec Info.addMoneyFromCard 50.000000,2,1
go
exec Info.addMoneyFromCard 50.000000,2,2
go
exec Info.addMoneyFromCard 50.000000,2,3
go
exec Info.ShowBalanceByUserID 1
go
exec Info.ShowBalanceByUserID 2
go


select * from [Info].[Transaction] AS T WHERE T.CurrencyID=1
GO
select * from [Info].[Transaction] AS T WHERE T.CurrencyID=2
GO
select * from [Info].[Transaction] AS T WHERE T.CurrencyID=3
go

--exec Info.SendMoney 12,2,1,2,1
go
--exec Info.SendMoney 12,2,2,1,1
go

--exec Info.SendMoney 10,2,1,2,2
go

USE PayMeDB
--@Balance as DECIMAL(12,6),@TransactionType as int,@SenderID as int,@ReceiverID as int,@CurrencyID as int)
go
/*
EXEC Info.SendMoney 10,2,1,2,1
go
EXEC Info.SendMoney 10,2,2,1,1
go
EXEC Info.SendMoney 10,2,1,2,3
go
exec Info.SendMoney 10,3,2,1,2
go
exec Info.SendMoney 10,3,2,1,2
go
exec Info.SendMoney 10,3,2,1,3
go
exec Info.SendMoney 10,3,1,2,1
go

exec Info.SendMoney 10,3,2,1,3
go

exec Info.SendMoney 10,3,1,2,3
go
*/
exec Info.SendMoney 10,2,1,2,1
go

SELECT *,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID
GO

SELECT *,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=2
GO

SELECT *,C.CurrencyType FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=1 AND T.ReceiverID=1 AND T.TransactionTypeID=1
GO

SELECT T.Balance FROM [Info].[Transaction] AS T join [Info].[Users] as U ON U.UserID=T.ReceiverID join [Info].[Currency] as C ON C.CurrencyID=T.CurrencyID WHERE C.CurrencyID=3 AND T.ReceiverID=1 AND T.TransactionTypeID=1
GO
USE PayMeDB

select * from Info.Users

select * from Info.TimeZones

select * from Info.SecurityQuest

select * from [Info].[BankInformation]

select * from [Info].[Transaction]
go

select Balance from [Info].[Transaction] where ReceiverID=1  AND CurrencyID=1
GO
select * from [Info].[Receipt]
go
select * from [Info].[DoneTransaction]
go



select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime) AS Time from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID WHERE T.TransactionTime > '2016.11.16' AND T.TransactionTime < '2016.12.17'
GO


create procedure Info.ShowInvoceBtwDate(@StartDate as datetime,@FinishDate as datetime)
as
select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime) AS Time from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID WHERE T.TransactionTime > @StartDate AND T.TransactionTime < @FinishDate
GO

exec Info.ShowInvoceBtwDate '2016-11-11','2016-12-18'
go


select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime)  AS Time,TT.TransactionType from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID join [Info].[TransactionType] as TT on TT.TransactionTypeID=T.TransactionTypeID WHERE (T.TransactionTypeID=2 OR T.TransactionTypeID=3) AND T.TransactionTime > '2016.11.16' AND T.TransactionTime < '2016.12.17' AND U.UserID=1
GO

/*create procedure Info.ShowInvoceBtwDateByID(@StartDate as datetime,@FinishDate as datetime,@UserID as int)
as
select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime)  AS Time,TT.TransactionType from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID join [Info].[TransactionType] as TT on
 TT.TransactionTypeID=T.TransactionTypeID WHERE (T.TransactionTypeID=2 OR T.TransactionTypeID=3) AND T.TransactionTime > @StartDate AND T.TransactionTime < @FinishDate AND U.UserID=@UserID
GO*/

create procedure Info.ShowInvoceBtwDateByID(@StartDate as datetime,@FinishDate as datetime,@UserID as int)
as
select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime)  AS Time,TT.TransactionType from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID join [Info].[TransactionType] as TT on TT.TransactionTypeID=T.TransactionTypeID 
WHERE (T.TransactionTypeID=2 OR T.TransactionTypeID=3) 
AND T.TransactionTime > @StartDate AND T.TransactionTime < @FinishDate AND U.UserID=@UserID
GO

create procedure Info.ShowActivity(@StartDate as datetime,@FinishDate as datetime,@UserID as int)
as
select TT.TransactionType,T.Balance, CONVERT(date, T.TransactionTime)  AS Time from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID join [Info].[TransactionType] as TT on TT.TransactionTypeID=T.TransactionTypeID 
WHERE (T.TransactionTypeID=2 OR T.TransactionTypeID=3) 
AND T.TransactionTime > @StartDate AND T.TransactionTime < @FinishDate AND U.UserID=@UserID
GO

select U.FirstName,U.LastName,U.SSN,T.Balance, CONVERT(date, T.TransactionTime)  AS Time,TT.TransactionType from [Info].[Users] as U join [Info].[Transaction] as T on T.ReceiverID=U.UserID join [Info].[TransactionType] as 
TT on TT.TransactionTypeID=T.TransactionTypeID WHERE (T.TransactionTypeID=3) 
AND T.TransactionTime > '2016-11-11' AND T.TransactionTime < '2016-12-18' AND U.UserID=1
GO

select TT.TransactionType,T.Balance, T.TransactionTime  AS Time from [Info].[Users] as U join [Info].[Transaction] as T on T.SenderID=U.UserID join [Info].[TransactionType] as TT on TT.TransactionTypeID=T.TransactionTypeID 
WHERE (T.TransactionTypeID=2 OR T.TransactionTypeID=3) 
AND T.TransactionTime > '2016-11-11' AND T.TransactionTime <  '2016-12-18' AND U.UserID=2
GO

exec Info.ShowInvoceBtwDateByID  '2016-11-11','2016-12-18',1
go
exec Info.ShowInvoceBtwDateByID  '2016-11-11','2016-12-18',2
go

exec Info.ShowActivity '2016-11-11','2016-12-18',1

exec Info.ShowActivity '2016-11-11','2016-12-18',2

go
exec Info.ShowBalanceByUserID 1
go
exec Info.ShowBalanceByUserID 2
go
